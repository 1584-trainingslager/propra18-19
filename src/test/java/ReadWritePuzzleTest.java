import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ReadWritePuzzleTest {

    @Test
    //@DisplayName("Read puzzle")
    public void testReadPuzzle() {
        Field puzzle = ReadWritePuzzle.readFromDisk("spielstand.bgs");
        assertAll(() -> assertEquals(5, puzzle.getWidth()),
                () -> assertEquals(6, puzzle.getHeight()),
                () -> assertEquals(6, puzzle.getNumberOfIslands()));
    }
}




