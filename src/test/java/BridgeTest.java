import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class BridgeTest {

    @Test
    //@DisplayName("Create bridge")
    public void testCreateBridge() {
        Bridge bridge = new Bridge(0, 3, false);
        assertAll(() -> assertEquals(0, bridge.getStartIndex()),
                () -> assertEquals(3, bridge.getEndIndex()),
                () -> assertEquals(false, bridge.isDoubleBridge()));
    }

    @Test
    //@DisplayName("Create double bridge")
    public void testCreateDoubleBridge() {
        Bridge bridge = new Bridge(4, 5, true);
        assertAll(() -> assertEquals(4, bridge.getStartIndex()),
                () -> assertEquals(5, bridge.getEndIndex()),
                () -> assertEquals(true, bridge.isDoubleBridge()));
    }
}