import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class IslandTest {

    @Test
    //@DisplayName("Create island")
    public void testCreateIsland() {
        Island isle = new Island(4, 5, 2);
        assertAll(() -> assertEquals(4, isle.getColumn()),
                () -> assertEquals(5, isle.getRow()),
                () -> assertEquals(2, isle.getNumberOfBridges()));
    }
}