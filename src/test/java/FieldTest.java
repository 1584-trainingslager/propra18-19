import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class FieldTest{

    @Test
    //@DisplayName("Create field")
    public void testCreateField() {
        Field puzzle = new Field(4, 5, 3);
        assertAll(() -> assertEquals(3, puzzle.getNumberOfIslands()),
                () -> assertEquals(4, puzzle.getWidth()),
                () -> assertEquals(5, puzzle.getHeight()));
    }

    @Test
    //@DisplayName("Create field with list of islands")
    public void testCreateFieldWithIslands() {
        List<Island> islands = new ArrayList<>();
        islands.add(new Island(0, 1, 2));
        Field puzzle = new Field(6, 7, islands, new ArrayList<Bridge>());

        assertAll(() -> assertEquals(1, puzzle.getNumberOfIslands()),
                () -> assertEquals(6, puzzle.getWidth()),
                () -> assertEquals(7, puzzle.getHeight()));
    }
}