package gui;

import java.awt.*;

public class MainWindow extends BaseFrame{

    public MainWindow() {
        Frame f = new BaseFrame();
        f.setLayout(new FlowLayout());
        f.setTitle("Student q123456");

        f.setMenuBar(CreateMenuBar());
        f.setVisible(true);
    }

    private MenuBar CreateMenuBar()
    {
        MenuBar mb = new MenuBar();
        mb.add(CreateFirstMenuBarEntry());
        return mb;
    }

    private Menu CreateFirstMenuBarEntry()
    {
        Menu menu = new Menu("Datei");
        MenuItem newPuzzle = new MenuItem("Neues Raetsel");
        MenuItem restartPuzzle = new MenuItem("Raetsel neu starten");
        MenuItem loadPuzzle = new MenuItem("Raetsel laden");
        MenuItem savePuzzle = new MenuItem("Raetsel speichern");
        MenuItem saveAsPuzzle = new MenuItem("Raetsel speichern unter");
        MenuItem exit = new MenuItem("Beenden");
        exit.addActionListener(new CloseFrame());
        menu.add(newPuzzle);
        menu.add(restartPuzzle);
        menu.add(loadPuzzle);
        menu.add(savePuzzle);
        menu.add(saveAsPuzzle);
        menu.add(exit);

        return menu;
    }
}
