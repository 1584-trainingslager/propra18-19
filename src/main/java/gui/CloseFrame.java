package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class CloseFrame extends WindowAdapter implements ActionListener {
    public void windowClosing(WindowEvent e){
        CloseFrame();
    }

    public void actionPerformed(ActionEvent actionEvent) {
        CloseFrame();
    }

    private void CloseFrame() {
        System.exit(0);
    }
}