package gui;

import java.awt.*;

public class BaseFrame extends Frame {
    public BaseFrame() {
        addWindowListener(new CloseFrame());
        setSize(300, 400);
        setLocationRelativeTo(null);
    }
}