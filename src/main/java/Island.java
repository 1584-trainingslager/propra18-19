public class Island {
    private int column;
    private int row;
    private int numberOfBridges;

    Island(int column, int row, int numberOfBridges) {
        this.column = column;
        this.row = row;
        this.numberOfBridges = numberOfBridges;
    }

    public int getColumn() {
        return column;
    }

    public int getRow() {
        return row;
    }

    public int getNumberOfBridges() {
        return numberOfBridges;
    }
}
