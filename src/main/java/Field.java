import java.util.ArrayList;
import java.util.List;

public class Field {
    private final int width;
    private final int height;
    private final int numberOfIslands;
    List<Island> islands;
    List<Bridge> bridges;

    Field(int width, int height, int numberOfIslands) {
        this.width = width;
        this.height = height;
        this.numberOfIslands = numberOfIslands;
    }

    Field(int width, int height, List<Island> islands, List<Bridge> bridges) {
        this(width, height, islands.size());
        this.islands = islands;
        this.bridges = bridges;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getNumberOfIslands() {
        return numberOfIslands;
    }
}
