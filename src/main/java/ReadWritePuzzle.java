import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class ReadWritePuzzle {

    public static void writeOnDisk(Field puzzle, String fileName) {
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(fileName, false));

            bw.write("Field\n");
            bw.write("# Width x Height | Number of islands\n");
            bw.write(puzzle.getWidth() + " x " + puzzle.getHeight() + " | " + puzzle.getNumberOfIslands());

            writeIslands(bw, puzzle.islands);
            writeBridges(bw, puzzle.bridges);

            bw.close();
        }
        catch (IOException e) {
            System.out.println("Error write on disk: " + e);
        }
    }

    public static Field readFromDisk(String filename) {
        int width = 0, height = 0, numberOfIslands = 0;
        ArrayList<Island> islands = new ArrayList<>();
        ArrayList<Bridge> bridges = new ArrayList<>();
        ArrayList<String> strIslands = new ArrayList<>();
        ArrayList<String> strBridges = new ArrayList<>();

        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(filename)));
            br.readLine();
            br.readLine();
            String line = br.readLine();
            String[] res = line.split("\\s*[x,|, ]+");
            width = Integer.parseInt(res[0]);
            height = Integer.parseInt(res[1]);
            numberOfIslands = Integer.parseInt(res[2]);
            line = br.readLine();

            boolean isle = false;
            boolean bridge = false;
            while (line != null) {
                if (line.contains("ISLANDS")) {
                    isle = true;
                    bridge = false;
                }
                if (line.contains("BRIDGES")) {
                    isle = false;
                    bridge = true;
                }
                if (line.startsWith("(") && isle)
                    strIslands.add(line);
                if (line.startsWith("(") && bridge)
                    strBridges.add(line);

                line = br.readLine();
            }

            for (String s : strIslands) {
                String[] split = s.split("\\s*[(,),|,, ]+");
                islands.add(new Island(Integer.parseInt(split[1]), Integer.parseInt(split[2]), Integer.parseInt(split[3])));
            }
            for (String s : strBridges) {
                String[] split = s.split("\\s*[(,),|,, ]+");
                bridges.add(new Bridge(Integer.parseInt(split[1]), Integer.parseInt(split[2]), Boolean.parseBoolean(split[3])));
            }
        }
        catch (FileNotFoundException e) {
            System.out.println("Error read file: " + e);
        }
        catch (IOException e) {
            System.out.println("Error read line: " + e);
        }

        Field puzzle = null;
        if (islands.size() == 0)
            puzzle = new Field(width, height, numberOfIslands);
        else
            puzzle = new Field(width, height, islands, bridges);

        return puzzle;
    }

    private static void writeIslands(BufferedWriter bw, List<Island> islands) throws IOException {
        if (islands == null)
            return;
        if (islands.size() == 0)
            return;

        bw.newLine();
        bw.newLine();
        bw.write("ISLANDS\n");
        bw.write("# { ( Column, Row | Number of bridges ) }\n");
        bw.write("# Columns and rows are 0-indexed!");
        for (Island isle : islands)
            bw.write(writePuzzleLine(isle.getColumn(), isle.getRow(), isle.getNumberOfBridges()));
    }

    private static void writeBridges(BufferedWriter bw, List<Bridge> bridges) throws IOException {
        if (bridges == null)
            return;
        if (bridges.size() == 0)
            return;

        bw.newLine();
        bw.newLine();
        bw.write("BRIDGES\n");
        bw.write("# { ( Start Index, End Index | Double Bridge ) }");
        for (Bridge bridge : bridges)
            bw.write(writePuzzleLine(bridge.getStartIndex(), bridge.getEndIndex(), bridge.isDoubleBridge()));
    }

    private static String writePuzzleLine(int a, int b, int c) {
        return "\n( " + a + ", " + b + " | " + c + " )";
    }

    private static String writePuzzleLine(int a, int b, boolean c) {
        return "\n( " + a + ", " + b + " | " + c + " )";
    }
}