public class Bridge {
    private int startIndex;
    private int endIndex;
    private boolean doubleBridge;

    Bridge(int startIndex, int endIndex, boolean doubleBridge) {
        this.startIndex = startIndex;
        this.endIndex = endIndex;
        this.doubleBridge = doubleBridge;
    }

    public int getStartIndex() {
        return startIndex;
    }

    public int getEndIndex() {
        return endIndex;
    }

    public boolean isDoubleBridge() {
        return doubleBridge;
    }
}
