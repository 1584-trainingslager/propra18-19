# Bridges oder (im  japanischen  Original) „Hashiwokakero“

Hierbei handelt es sich um ein Rätsel, welches der Spieler durch logische Überlegungen lösen soll.
 
Die Aufgabe umfasst  
* Programmierung eines „Spielfeldes“, mit dem der Benutzer Spielzüge vornehmen kann
* Implementierung von Algorithmen zur Erzeugung neuer Rätsel oder der Ermittlung eines Vorschlags für einen nächsten Spielzug
* Schnittstellen zum Laden und Speichern von Rätseln zu implementieren

Die konkreten Anforderungen können hier nach gelesen werden <https://gitlab.com/1584-trainingslager/propra18-19/-/blob/main/2018_2019_ws_aufgabenstellung.pdf>
